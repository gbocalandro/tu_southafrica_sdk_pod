Pod::Spec.new do |s|

s.pod_target_xcconfig = {
    'EXCLUDED_ARCHS[sdk=iphonesimulator*]' => 'arm64'
  }
  s.user_target_xcconfig = { 'EXCLUDED_ARCHS[sdk=iphonesimulator*]' => 'arm64' }

  s.name         = "TransunionSDKSA"
  s.version      = "0.0.26"
  s.summary      = "TU South Africa SDK"
  s.description  = <<-DESC
  una descripcion cualquiera
                   DESC

  s.homepage     = "mipagina.com"
  s.license      =  "Copyright @Santiago"

  s.author             = { "Santiago Linietsky" => "santiagolky@gmail.com" }
  s.ios.deployment_target = '11.0'
  s.ios.vendored_frameworks = 'TransunionSDK.framework'
  s.source            = { :http => 'https://gitlab.com/gbocalandro/tu_southafrica_sdk_pod/-/raw/3c0414376dd4a874f63e72c241b29a8e4db75828/TransunionSDK.framework.zip' }
  s.exclude_files = "Classes/Exclude"

end
